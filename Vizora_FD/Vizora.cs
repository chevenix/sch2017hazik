﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizora_FD
{
    class Vizora
    {
        private string _nev;
        public string Nev
        {
            get { return _nev; }
        }

        private double _allas;
        public double Allas
        {
            get { return _allas; }
            set
            {
                _korabbiak.Add(new OraAllas(_allas, _beallitas));
                _allas = value;
                _beallitas = DateTime.Now;
            }
        }

        private DateTime _beallitas;
        public DateTime Beallitas
        {
            get { return _beallitas; }
        }

        private List<OraAllas> _korabbiak;
        public List<OraAllas> Korabbiak
        {
            get { return _korabbiak; }
        }

        public Vizora(string nev)
        {
            _nev = nev;
            //_allas = 0.0;
            _korabbiak = new List<OraAllas>();
        }


        public string Kiir()
        {
            StringBuilder sb = new StringBuilder(Nev);
            sb.Append(" óra, ");
            sb.Append(Allas.ToString("F1"));
            sb.Append(" m3, ");
            sb.AppendLine(Beallitas.ToString());
            sb.AppendLine("Korábbi állások:");
            for (int i = _korabbiak.Count - 1; i >= 0; i--)
            {
                sb.Append(_korabbiak[i].Allas.ToString("F1") 
                    + " m3" + _korabbiak[i].AllasDatuma + Environment.NewLine);
                //sb.AppendFormat("{0} m3{1}", _korabbiak[i].Allas.ToString("F1"), Environment.NewLine);
            }
            return sb.ToString();
        }

    }
}
