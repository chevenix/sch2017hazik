﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizora_FD
{
    class Program
    {
        static void Main()
        {
            List<Vizora> orak = new List<Vizora>();
            Vizora kivalasztott = null;

            while (true)
            {
                Console.WriteLine("Vízóra [h]ozzáadása");
                Console.WriteLine("Vízóra [k]iválasztása");

                string input = Console.ReadLine();
                if (input == "h")
                {
                    Console.Write("Az új óra neve: ");
                    input = Console.ReadLine();
                    Vizora uj = new Vizora(input);
                    orak.Add(uj);
                }
                else if (input == "k")
                {
                    Console.Write("Az óra neve: ");
                    input = Console.ReadLine();
                    kivalasztott = null;
                    foreach (Vizora vizora in orak)
                    {
                        if (vizora.Nev == input)
                        {
                            kivalasztott = vizora;
                            break;
                        }
                    }
                    if (kivalasztott != null)
                    {
                        Console.WriteLine("Megvan!");
                        Console.WriteLine(kivalasztott.Kiir());
                        //TODO
                    }
                    else
                    {
                        Console.WriteLine("Nincs ilyen óra.");
                    }
                }
                else
                {
                    break;
                }
                Console.Clear();
            }

            Console.ReadLine();
        }
    }
}