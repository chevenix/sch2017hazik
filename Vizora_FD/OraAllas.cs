﻿using System;

namespace Vizora_FD
{
    class OraAllas
    {
        private double _allas;
        public double Allas
        {
            get { return _allas; }
        }

        private DateTime _allasDatuma;
        public DateTime AllasDatuma
        {
            get { return _allasDatuma; }
        }

        public OraAllas(double allas, DateTime allasDatuma)
        {
            _allas = allas;
            _allasDatuma = allasDatuma;
        }
    }
}