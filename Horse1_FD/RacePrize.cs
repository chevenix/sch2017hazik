﻿namespace Horse1_FD
{
    internal class RacePrize
    {
        private string _name;
        public string Name
        {
            get { return _name; }
        }

        public RacePrize(string name)
        {
            _name = name;
        }
    }
}