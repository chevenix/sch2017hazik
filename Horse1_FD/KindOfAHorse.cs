﻿using System;

namespace Horse1_FD
{
    abstract class KindOfAHorse
    {
        protected string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        protected DateTime _birthDate;
        public DateTime BirthDate
        {
            get { return _birthDate; }
        }

        protected int? _price;
        public int? Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public virtual void Whine()
        {
            Console.WriteLine("NYIHAHA");
        }
    }
}
