﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horse1_FD
{
    class Program
    {
        static void Main()
        {
            List<Horse> horses = new List<Horse>();
            horses.Add(new Horse("Overdose", new DateTime(2000,1,1), 9000));
            horses.Add(new Horse("Kincsem", new DateTime(2001,1,1), 12000));
            horses.Add(new Horse("Override", new DateTime(1999,1,1), 11000));

            //horses.Sort(new HorsePriceComparer());
            //horses.Sort(Horse.ComparePrices);
            horses.Sort(delegate(Horse x, Horse y)
            {
                if (x.Price.HasValue == false) return -1;
                if (y.Price.HasValue == false) return 1;
                return x.Price.Value.CompareTo(y.Price.Value);
            });
            foreach (Horse horse in horses)
            {
                Console.WriteLine(horse.Name);
            }

            Console.WriteLine("Drágák:");
            foreach (Horse horse in horses)
            {
                if (horse.Price > 10000) Console.WriteLine(horse.Name);
            }

            Console.ReadLine();
        }
    }

    class HorsePriceComparer : IComparer<Horse>
    {
        public int Compare(Horse x, Horse y)
        {
            if (x.Price.HasValue == false)
            {
                return -1;
            }
            if (y.Price.HasValue == false)
            {
                return 1;
            }
            return x.Price.Value.CompareTo(y.Price.Value);
        }
    }
}