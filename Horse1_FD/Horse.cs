﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Horse1_FD
{
    class Horse : KindOfAHorse, IComparable<Horse>
    {
        

        private List<RacePrize> _prizes = new List<RacePrize>();

        public List<RacePrize> Prizes
        {
            get { return _prizes; }
        }

        public event EventHandler Race;

        public void EnterRace(RacePrize prize)
        {
            if (Race != null)
            {
                Race(this, EventArgs.Empty);
            }
            if (new Random().Next(0,10) > 5)
            {
                Console.WriteLine("{0} nyert!", _name);
                this.Prizes.Add(prize);
            }
        }

        public Horse(string name, DateTime birthDate)
        {
            _name = name;
            _birthDate = birthDate;
            _price = 1000;
        }


        public Horse(string name, DateTime birthDate, int price) 
            : this(name, birthDate)
        {
            _price = price;
        }

        public int CompareTo(Horse other)
        {
            return Name.CompareTo(other.Name);
        }

        public static int ComparePrices(Horse x, Horse y)
        {
            if (x.Price.HasValue == false)
            {
                return -1;
            }
            if (y.Price.HasValue == false)
            {
                return 1;
            }
            return x.Price.Value.CompareTo(y.Price.Value);
        }
    }
}