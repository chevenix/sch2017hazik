﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Horse1_FD
{
    class Sausage
    {
        private int _price;
        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public static Sausage Grind(KindOfAHorse k)
        {
            Sausage s = new Sausage(k);
            return s;
        }

        public Sausage(int price)
        {
            _price = price;
        }

        public Sausage(KindOfAHorse k)
        {
            this.Price = k.Price.HasValue ? k.Price.Value : 0;
        }

        public static explicit operator Sausage(KindOfAHorse k)
        {
            return new Sausage(k);
        }

    }
}
