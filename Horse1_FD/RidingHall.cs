﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horse1_FD
{
    class RidingHall
    {
        private List<KindOfAHorse> _things = new List<KindOfAHorse>();

        public void Add(KindOfAHorse x)
        {
            _things.Add(x);
        }

        public void GivesSugar()
        {
            foreach (KindOfAHorse x in _things)
            {
                x.Whine();
            }
        }
    }
}
