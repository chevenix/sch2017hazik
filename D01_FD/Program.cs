﻿using System;

namespace D01_FD
{
    class Program
    {
        static void Main()
        {
            Console.Write("Számot! ");
            string s = Console.ReadLine();
            int i = int.Parse(s);
            if (i < 1 || i > 5) return;
            else
            {
                Par[] parok = new Par[i];
                for (int j = 0; j < parok.Length; j++)
                {
                    parok[j] = new Par();
                    parok[j].X = j + 1;
                    parok[j].Y = j * j;
                }
                Console.WriteLine("X: {0}, Y: {1}", parok[0].X, parok[0].Y);
                Console.ReadLine();
            }



            Console.ReadLine();
        }
    }
}