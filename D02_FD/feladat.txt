﻿Második (2017. 03. 31.)
Készíts egy konzolalkalmazást, amelynek neve legyen: D02_<monogramod>! (Pl.: D02_FD) A program működése a következő:
1.	Hozd létre a Food nevű osztályt. Tartalmazzon egy tulajdonságot: int Amount, ami 0-nál kisebb értéket ne vehessen fel. Ez dobjon fel egy eseményt (Empty), ha 0-ra csökkent az értéke. Itt ügyelj arra, hogy ha nincs feliratkozó, ne szálljon el!
a.	Az esemény szignatúráját rád bízom.
2.	Hozd létre a Drink nevű osztályt, amely szintén rendelkezzen egy int Amounttal.
3.	A Main metódusban:
a.	készíts egy listát, amiben legyen egy Food és egy Drink, 1-1-re állított Amountokkal.
b.	Csökkentsd le az Amountokat két alkalommal. (ciklus…)
c.	Írd ki mind a két alkalommal az új értékeket.
d.	A Foodon legyen egy feliratkozó, ami a fentiek szerint egyszer kell, hogy kiírjon valamit.
4.	Saját típusaid közül csak a Foodból és a Drinkből lehessen létrehozni példányokat. (Természetesen, ha készítettél saját delegáltat, vagy eventargsot, az kivétel a szabály alól.)
A programot megnézem mindenkinek a gépénél, nem kell elküldeni/feltölteni.
