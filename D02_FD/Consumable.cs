﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D02_FD
{
    abstract class Consumable
    {
        private int _amount;
        public virtual int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

    }
}