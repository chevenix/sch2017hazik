﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D02_FD
{
    class Program
    {
        static void Main()
        {
            List<Consumable> items = new List<Consumable>();
            Food f = new Food();
            f.Amount = 1;

            Drink d = new Drink();
            d.Amount = 1;

            items.Add(f);
            items.Add(d);

            Subscriber s = new Subscriber();
            f.Empty += s.Subscribe;

            foreach (Consumable item in items)
            {
                item.Amount--;
                Console.WriteLine(item.Amount);
                //if (item is Food)
                //{
                //    ((Food)item).Amount--;
                //    Console.WriteLine(((Food)item).Amount);
                //}
                //else if (item is Drink)
                //{
                //    ((Drink)item).Amount--;
                //    Console.WriteLine(((Drink)item).Amount);
                //}
            }
            foreach (Consumable item in items)
            {
                item.Amount--;
                Console.WriteLine(item.Amount);
            }


            Console.ReadLine();
        }
    }
}